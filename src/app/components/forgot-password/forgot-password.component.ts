import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  @Input() recoveryText: string = "";
  isSent: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  send(): void {
    this.isSent = true;
  }

}
