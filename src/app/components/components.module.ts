import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxEchartsModule } from 'ngx-echarts';
import { ComponentsRoutingModule } from './components-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { StorePendingRegistrationComponent } from './store-pending-registration/store-pending-registration.component';
import { StoreRegistrationDetailComponent } from './store-registration-detail/store-registration-detail.component';
import { StoreStatusComponent } from './store-status/store-status.component';
import { StoreGenerateReportComponent } from './store-generate-report/store-generate-report.component';



@NgModule({
  declarations: [
    LoginComponent,
    ForgotPasswordComponent,
    PasswordResetComponent,
    NotFoundComponent,
    DashboardComponent,
    LogoutComponent,
    StorePendingRegistrationComponent,
    StoreRegistrationDetailComponent,
    StoreStatusComponent,
    StoreGenerateReportComponent
  ],
  imports: [
    CommonModule,
    ComponentsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxEchartsModule
  ]
})
export class ComponentsModule { }
