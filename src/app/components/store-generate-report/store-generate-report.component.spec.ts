import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreGenerateReportComponent } from './store-generate-report.component';

describe('StoreGenerateReportComponent', () => {
  let component: StoreGenerateReportComponent;
  let fixture: ComponentFixture<StoreGenerateReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreGenerateReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreGenerateReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
