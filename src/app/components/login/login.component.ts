import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginForm: LoginForm = new LoginForm();

  constructor(private router: Router, private route: ActivatedRoute, private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
  }

  login(): void {
    this.authenticationService.login(this.loginForm);
    // this.router.navigateByUrl('/dashboard');
  }
}

export class LoginForm {
  username: string = "";
  password: string = "";
}
