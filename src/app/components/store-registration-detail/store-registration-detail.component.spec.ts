import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreRegistrationDetailComponent } from './store-registration-detail.component';

describe('StoreRegistrationDetailComponent', () => {
  let component: StoreRegistrationDetailComponent;
  let fixture: ComponentFixture<StoreRegistrationDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreRegistrationDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreRegistrationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
