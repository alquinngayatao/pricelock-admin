import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../shared/services/auth-guard.service';
import { RoleGuardService } from '../shared/services/role-guard.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { StoreGenerateReportComponent } from './store-generate-report/store-generate-report.component';
import { StorePendingRegistrationComponent } from './store-pending-registration/store-pending-registration.component';
import { StoreRegistrationDetailComponent } from './store-registration-detail/store-registration-detail.component';
import { StoreStatusComponent } from './store-status/store-status.component';

const routes: Routes = [
  {
    path: 'login', component: LoginComponent
  },
  {
    // path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService, RoleGuardService],
    // data: { expectedRole: 'user' }
    path: 'dashboard', component: DashboardComponent
  },
  {
    // path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService, RoleGuardService],
    // data: { expectedRole: 'user' }
    path: 'store-pending-registration', component: StorePendingRegistrationComponent
  },
  {
    // path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService, RoleGuardService],
    // data: { expectedRole: 'user' }
    path: 'store-registration-detail/:id', component: StoreRegistrationDetailComponent
  },
  {
    // path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService, RoleGuardService],
    // data: { expectedRole: 'user' }
    path: 'store-status/:id', component: StoreStatusComponent
  },
  {
    // path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService, RoleGuardService],
    // data: { expectedRole: 'user' }
    path: 'store-generate-report/:id', component: StoreGenerateReportComponent
  },
  {
    path: 'forgot-password', component: ForgotPasswordComponent
  },
  {
    path: 'password-reset/:id', component: PasswordResetComponent
  },
  {
    path: '**', component: NotFoundComponent
  },
  {
    path: 'logout', component: LogoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule { }
