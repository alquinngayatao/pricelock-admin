import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StorePendingRegistrationComponent } from './store-pending-registration.component';

describe('StorePendingRegistrationComponent', () => {
  let component: StorePendingRegistrationComponent;
  let fixture: ComponentFixture<StorePendingRegistrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StorePendingRegistrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StorePendingRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
