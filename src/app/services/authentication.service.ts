import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { LoginForm } from '../components/login/login.component';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(private http: HttpClient, public router: Router, private toastr: ToastrService) {
  }
  
  login(loginForm: LoginForm): void {
    this.http.post<any>("http://localhost:8080/authenticate", loginForm)
      .subscribe(data => {
        localStorage.setItem('token', data?.jwtToken);
        this.toastr.success("Welcome back", "Login Success");
        this.router.navigate(['dashboard']);
      });
  }
}
